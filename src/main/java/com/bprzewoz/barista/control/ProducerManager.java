package com.bprzewoz.barista.control;

import com.bprzewoz.barista.config.ProducerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerManager {
    public static final int INTERVAL = 1000;

    private Producer<?, ?> producer;

    public static ProducerManager from(ProducerConfig producerConfig) {
        return from(producerConfig.getProducerProperties(), producerConfig.getValueClass());
    }

    public static <T> ProducerManager from(Properties properties, Class<T> valueClass) {
        return new ProducerManager(new KafkaProducer<String, T>(properties));
    }

    public ProducerManager(Producer<?, ?> producer) {
        this.producer = producer;
    }

    public void send(ProducerRecord producerRecord, int duration, int capacity) {
        long startTime;
        long endTime;
        long elapsedTime;

        for (int i = 0; i < duration; i++) {
            startTime = System.currentTimeMillis();
            for (int j = 0; j < capacity; j++) {
                send(producerRecord);
            }
            endTime = System.currentTimeMillis();
            elapsedTime = endTime - startTime;

            if (elapsedTime < INTERVAL) {
                try {
                    Thread.sleep(INTERVAL - elapsedTime);
                } catch (InterruptedException e) {
                    throw new IllegalStateException();
                }
            }
        }
    }

    private void send(ProducerRecord producerRecord) {
        producer.send(producerRecord, (metadata, exception) -> {
            if (exception != null) {
                throw new KafkaException("Asynchronous send failed.", exception);
            }
        });
    }
}