package com.bprzewoz.barista.control;

import javafx.animation.AnimationTimer;
import javafx.scene.control.Label;

public class StatisticsTimer extends AnimationTimer {
    private final Label sendedLabel;
    private final Label timeLabel;
    private final int duration;
    private final int capacity;

    private long checkpoint;
    private int seconds;

    public StatisticsTimer(Label sendedLabel, Label timeLabel, int duration, int capacity) {
        this.sendedLabel = sendedLabel;
        this.timeLabel = timeLabel;
        this.duration = duration;
        this.capacity = capacity;
    }

    @Override
    public void start() {
        super.start();
        this.checkpoint = System.nanoTime();
        this.seconds = 1;
        updateLabels(seconds);
    }

    @Override
    public void handle(long now) {
        if (now > checkpoint + 1_000_000_000) {
            checkpoint = now;
            updateLabels(++seconds);
        }
    }

    private void updateLabels(int seconds) {
        updateSendedLabel(seconds);
        updateTimeLabel(seconds);
    }

    private void updateSendedLabel(int seconds) {
        sendedLabel.setText("Sended: " + (seconds * capacity) + " / " + (duration * capacity));
    }

    private void updateTimeLabel(int seconds) {
        timeLabel.setText("Time: " + seconds + " / " + duration + " [s]");
    }
}
