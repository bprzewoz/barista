package com.bprzewoz.barista.control;

import com.bprzewoz.barista.config.BaristaConfig;
import com.bprzewoz.barista.config.CaseConfig;
import com.bprzewoz.barista.config.DefaultValues;
import com.bprzewoz.barista.config.ProducerConfig;
import com.jfoenix.controls.*;
import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import java.io.PrintWriter;
import java.io.StringWriter;

import static com.bprzewoz.barista.util.RecordUtils.serialize;
import static com.bprzewoz.barista.util.StringUtils.isNotBlank;

public class BaristaController {
    public static final String APP_CONF = "./config/app.conf";

    private AnimationTimer animationTimer;
    private Task<Void> task;
    private BaristaConfig baristaConfig;
    @FXML
    private JFXComboBox<String> casesComboBox;
    @FXML
    private JFXCheckBox schemaCheckBox;
    @FXML
    private JFXTextArea messageTextArea;
    @FXML
    private JFXTextField bootstrapServersTextField;
    @FXML
    private JFXTextField topicTextField;
    @FXML
    private JFXTextField sslPathTextField;
    @FXML
    private JFXCheckBox enableSslCheckButton;
    @FXML
    private JFXTextField durationTextField;
    @FXML
    private JFXTextField capacityTextField;
    @FXML
    private JFXButton sendButton;
    @FXML
    private JFXButton stopButton;
    @FXML
    private JFXButton saveButton;
    @FXML
    private JFXButton dropButton;
    @FXML
    private Label sendedLabel;
    @FXML
    private Label timeLabel;

    public BaristaController() {
        baristaConfig = BaristaConfig.from(APP_CONF);
    }

    @FXML
    private void initialize() {
        DefaultValues defaultValues = baristaConfig.getDefaultValues();
        casesComboBox.setItems(FXCollections.observableList(baristaConfig.getNames()));
        schemaCheckBox.setDisable(true);

        bootstrapServersTextField.setText(defaultValues.getBootstrapServers());
        topicTextField.setText(defaultValues.getTopic());

        sslPathTextField.setText(defaultValues.getSslPath());
        sslPathTextField.setDisable(!defaultValues.isEnableSsl());

        enableSslCheckButton.setSelected(defaultValues.isEnableSsl());
        enableSslCheckButton.selectedProperty().addListener((observable, oldValue, newValue) ->
                sslPathTextField.setDisable(!enableSslCheckButton.isSelected()));

        durationTextField.setText(defaultValues.getDuration());
        capacityTextField.setText(defaultValues.getCapacity());
    }

    @FXML
    protected void updateView() {
        String name = casesComboBox.getValue();
        CaseConfig caseConfig = baristaConfig.getCaseConfig(name);
        DefaultValues defaultValues = baristaConfig.getDefaultValues();

        updateSchemaCheckBox(caseConfig);
        updateMessageTextArea(caseConfig);
        updateTopicTextField(caseConfig, defaultValues);
    }

    private void updateMessageTextArea(CaseConfig caseConfig) {
        messageTextArea.setText(caseConfig.getMessage());
    }

    private void updateSchemaCheckBox(CaseConfig caseConfig) {
        boolean hasSchema = caseConfig.getSchema() != null;
        schemaCheckBox.setSelected(hasSchema);
        schemaCheckBox.setDisable(!hasSchema);
    }

    private void updateTopicTextField(CaseConfig caseConfig, DefaultValues defaultValues) {
        String defaultTopic = defaultValues.getTopic();
        String topic = caseConfig.getTopic();
        String text = isNotBlank(topic) ? topic : defaultTopic;
        topicTextField.setText(text);
    }

    @FXML
    protected void send() {
        if (schemaCheckBox.isSelected()) {
            send(generateRecord());
        } else {
            send(messageTextArea.getText());
        }
    }

    private GenericRecord generateRecord() {
        String name = casesComboBox.getValue();
        String message = messageTextArea.getText();
        Schema schema = baristaConfig.getCaseConfig(name).getSchema();
        JsonAvroConverter jsonAvroConverter = new JsonAvroConverter();
        return jsonAvroConverter.convertToGenericDataRecord(message.getBytes(), schema);
    }

    private void send(GenericRecord genericRecord) {
        String topic = topicTextField.getText();
        byte[] bytes = serialize(genericRecord);
        ProducerRecord<String, byte[]> producerRecord = new ProducerRecord<>(topic, bytes);
        send(producerRecord, ProducerConfig.Serializer.BYTE_ARRAY_SERIALIZER);
    }

    private void send(String string) {
        String topic = topicTextField.getText();
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topic, string);
        send(producerRecord, ProducerConfig.Serializer.STRING_SERIALIZER);
    }

    private void send(ProducerRecord producerRecord, ProducerConfig.Serializer serializer) {
        ProducerManager producerManager = createProducerManager(serializer);
        int duration = Integer.valueOf(durationTextField.getText());
        int capacity = Integer.valueOf(capacityTextField.getText());
        animationTimer = new StatisticsTimer(sendedLabel, timeLabel, duration, capacity);
        task = new Task<Void>() {
            @Override
            protected Void call() {
                disableView();
                try {
                    producerManager.send(producerRecord, duration, capacity);
                } catch (RuntimeException e) {
                    showAlert(Alert.AlertType.WARNING, e);
                } finally {
                    animationTimer.stop();
                    enableView();
                }
                return null;
            }
        };
        animationTimer.start();
        new Thread(task).start();
    }

    private ProducerManager createProducerManager(ProducerConfig.Serializer serializer) {
        boolean enableSslConfigFile = enableSslCheckButton.isSelected();
        String bootstrapServers = bootstrapServersTextField.getText();

        ProducerConfig producerConfig = ProducerConfig.from(bootstrapServers, serializer);
        if (enableSslConfigFile) {
            producerConfig.addSslProperties(sslPathTextField.getText());
        }
        return ProducerManager.from(producerConfig);
    }

    private void disableView() {
        setDisableView(true);
    }

    private void enableView() {
        setDisableView(false);
        String name = casesComboBox.getValue();
        CaseConfig caseConfig = baristaConfig.getCaseConfig(name);
        schemaCheckBox.setSelected(caseConfig.getSchema() != null);

        boolean hasSslPath = isNotBlank(sslPathTextField.getText());
        enableSslCheckButton.setDisable(hasSslPath);
        sslPathTextField.setDisable(!hasSslPath);
    }

    private void setDisableView(boolean value) {
        casesComboBox.setDisable(value);
        schemaCheckBox.setDisable(value);
        messageTextArea.setDisable(value);
        bootstrapServersTextField.setDisable(value);
        topicTextField.setDisable(value);
        sslPathTextField.setDisable(value);
        enableSslCheckButton.setDisable(value);
        durationTextField.setDisable(value);
        capacityTextField.setDisable(value);
        sendButton.setDisable(value);
        stopButton.setDisable(!value);
        saveButton.setDisable(value);
        dropButton.setDisable(value);
    }

    @FXML
    protected void stop() {
        task.cancel();
        task = null;
        enableView();
    }

    @FXML
    protected void save() {
        String name = casesComboBox.getValue();
        String message = messageTextArea.getText();
        baristaConfig.changeMessage(name, message);
    }

    @FXML
    protected void drop() {
        String name = casesComboBox.getValue();
        String message = baristaConfig.removeMessage(name);
        messageTextArea.setText(message);
    }

    private void showAlert(Alert.AlertType alertType, Exception e) {
        Alert alert = new Alert(alertType);
        alert.setTitle(alertType.toString());
        alert.setHeaderText(e.getClass().getSimpleName());
        alert.setContentText(e.getMessage());

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String exceptionText = stringWriter.toString();

        Label label = new Label("The exception stacktrace was:");
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.setStyle("-fx-background-color: #FFFFFF;");
        dialogPane.lookup(".content.label").setStyle("-fx-font-weight: bold;");

        ButtonBar buttonBar = (ButtonBar) alert.getDialogPane().lookup(".button-bar");
        buttonBar.getButtons().forEach(b -> {
            if (b.toString().contains("OK")) {
                b.setVisible(false);
            }
        });
        alert.showAndWait();
    }
}
