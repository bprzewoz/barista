package com.bprzewoz.barista.control;

import org.apache.avro.LogicalTypes;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericArray;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

public class ObjectGenerator {
    private static final Random random = new Random();

    private final int length;

    public ObjectGenerator() {
        this(1);
    }

    public ObjectGenerator(int length) {
        this.length = length;
    }

    public Object generateJson(Schema schema) {
        switch (schema.getType()) {
            case RECORD:
                return generateRecord(schema);
            case MAP:
                return generateMap(schema);
            case ARRAY:
                return generateArray(schema);
            case ENUM:
                return generateEnum(schema);
            case UNION:
                return generateUnion(schema);
            case FIXED:
                return generateFixed(schema);
            case STRING:
                return generateString(schema);
            case BYTES:
                return generateBytes(schema);
            case DOUBLE:
                return generateDouble(schema);
            case FLOAT:
                return generateFloat(schema);
            case LONG:
                return generateLong(schema);
            case INT:
                return generateInt(schema);
            case BOOLEAN:
                return generateBoolean(schema);
            case NULL:
                return null;
            default:
                throw new IllegalArgumentException("Unknown type: " + schema);
        }
    }

    private Object generateRecord(Schema schema) {
        GenericRecord genericRecord = new GenericData.Record(schema);
        Iterator<Schema.Field> iterator = schema.getFields().iterator();
        while (iterator.hasNext()) {
            Schema.Field field = iterator.next();
            Object value = generateJson(field.schema());
            genericRecord.put(field.name(), value);
        }
        return genericRecord;
    }

    protected Object generateMap(Schema schema) {
        Map<String, Object> map = new HashMap(length);
        for (int i = 0; i < length; i++) {
            Schema type = schema.getValueType();
            Object object = generateJson(type);
            map.put("key" + i, object);
        }
        return map;
    }

    private Object generateArray(Schema schema) {
        GenericArray<Object> genericArray = new GenericData.Array(length, schema);
        for (int i = 0; i < length; ++i) {
            Schema type = schema.getElementType();
            Object object = generateJson(type);
            genericArray.add(object);
        }
        return genericArray;
    }

    protected Object generateEnum(Schema schema) {
        List<String> enumSymbols = schema.getEnumSymbols();
        int index = random.nextInt(enumSymbols.size());
        return enumSymbols.get(index);
    }

    protected Object generateUnion(Schema schema) {
        Schema unionType = null;
        for (Schema type : schema.getTypes()) {
            if (type.getType() != Schema.Type.NULL) {
                unionType = type;
                break;
            }
        }
        return generateJson(unionType);
    }

    protected Object generateFixed(Schema schema) {
        return new GenericData.Fixed(schema, new byte[length]);
    }

    protected Object generateString(Schema schema) {
        return "string";
    }

    protected Object generateBytes(Schema schema) {
        int scale = (int) schema.getObjectProp("scale");
        return BigDecimal.valueOf(0, scale).toPlainString();
    }

    protected Object generateDouble(Schema schema) {
        return random.nextDouble();
    }

    protected Object generateFloat(Schema schema) {
        return random.nextFloat();
    }

    protected Object generateLong(Schema schema) {
        return System.currentTimeMillis();
    }

    protected Object generateInt(Schema schema) {
        if (Objects.equals(schema.getLogicalType(), LogicalTypes.date())) {
            LocalDate localDate = LocalDate.now();
            return localDate.toEpochDay();
        } else {
            return random.nextInt(100);
        }
    }

    protected Object generateBoolean(Schema schema) {
        return random.nextBoolean();
    }
}
