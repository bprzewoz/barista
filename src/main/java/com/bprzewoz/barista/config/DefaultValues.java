package com.bprzewoz.barista.config;

import com.typesafe.config.Config;

import java.util.Objects;

import static com.bprzewoz.barista.util.ConfigUtils.getValueOrNull;

public class DefaultValues {
    public static final String BOOTSTRAP_SERVERS = "bootstrapServers";
    public static final String TOPIC = "topic";
    public static final String SSL_PATH = "sslPath";
    public static final String ENABLE_SSL = "enableSsl";
    public static final String DURATION = "duration";
    public static final String CAPACITY = "capacity";

    private final String bootstrapServers;
    private final String topic;
    private final String sslPath;
    private final boolean enableSsl;
    private final String duration;
    private final String capacity;

    public static DefaultValues from(Config config) {
        return new DefaultValues(
                getValueOrNull(config, BOOTSTRAP_SERVERS),
                getValueOrNull(config, TOPIC),
                getValueOrNull(config, SSL_PATH),
                Boolean.TRUE.equals(getValueOrNull(config, ENABLE_SSL)),
                getValueOrNull(config, DURATION),
                getValueOrNull(config, CAPACITY)
        );
    }

    public DefaultValues(String bootstrapServers, String topic, String sslPath, boolean enableSsl, String duration, String capacity) {
        this.bootstrapServers = bootstrapServers;
        this.topic = topic;
        this.sslPath = sslPath;
        this.enableSsl = enableSsl;
        this.duration = duration;
        this.capacity = capacity;
    }

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public String getTopic() {
        return topic;
    }

    public String getSslPath() {
        return sslPath;
    }

    public boolean isEnableSsl() {
        return enableSsl;
    }

    public String getDuration() {
        return duration;
    }

    public String getCapacity() {
        return capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultValues that = (DefaultValues) o;
        return enableSsl == that.enableSsl &&
                Objects.equals(bootstrapServers, that.bootstrapServers) &&
                Objects.equals(topic, that.topic) &&
                Objects.equals(sslPath, that.sslPath) &&
                Objects.equals(duration, that.duration) &&
                Objects.equals(capacity, that.capacity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bootstrapServers, topic, sslPath, enableSsl, duration, capacity);
    }

    @Override
    public String toString() {
        return "DefaultValues{" +
                "bootstrapServers='" + bootstrapServers + '\'' +
                ", topic='" + topic + '\'' +
                ", sslPath='" + sslPath + '\'' +
                ", enableSsl=" + enableSsl +
                ", duration='" + duration + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }
}
