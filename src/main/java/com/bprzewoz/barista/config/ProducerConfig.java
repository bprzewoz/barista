package com.bprzewoz.barista.config;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.common.config.SslConfigs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ProducerConfig {
    public static final String PLAINTEXT = "PLAINTEXT";
    public static final String SSL = "SSL";

    private final Properties producerProperties;
    private final Class<?> valueClass;

    public static ProducerConfig from(String bootstrapServers, Serializer serializer) {
        Properties properties = new Properties();
        properties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, PLAINTEXT);
        properties.put(org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(org.apache.kafka.clients.producer.ProducerConfig.ACKS_CONFIG, "all");
        properties.put(org.apache.kafka.clients.producer.ProducerConfig.RETRIES_CONFIG, 0);
        properties.put(org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, Serializer.STRING_SERIALIZER.getClassName());
        properties.put(org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, serializer.getClassName());
        return new ProducerConfig(properties, serializer.getOutputClass());
    }

    public ProducerConfig(Properties producerProperties, Class<?> valueClass) {
        this.producerProperties = producerProperties;
        this.valueClass = valueClass;
    }

    public void addSslProperties(String sslPropertiesPath) {
        try (FileInputStream fileInputStream = new FileInputStream(sslPropertiesPath)) {
            Properties sslProperties = new Properties();
            sslProperties.load(fileInputStream);
            addSslProperties(sslProperties);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Missing ssl properties file: " + sslPropertiesPath, e);
        } catch (IOException e) {
            throw new IllegalStateException("Adding sll properties failed: " + sslPropertiesPath, e);
        }
    }

    public void addSslProperties(Properties sslProperties) {
        producerProperties.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SSL);
        producerProperties.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, sslProperties.getProperty(SslConfigs.SSL_KEY_PASSWORD_CONFIG));

        producerProperties.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, sslProperties.getProperty(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG));
        producerProperties.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, sslProperties.getProperty(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG));

        producerProperties.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, sslProperties.getProperty(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG));
        producerProperties.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, sslProperties.getProperty(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG));
    }

    public Properties getProducerProperties() {
        Properties properties = new Properties();
        properties.putAll(this.producerProperties);
        return properties;
    }

    public Class<?> getValueClass() {
        return valueClass;
    }

    public enum Serializer {
        STRING_SERIALIZER("org.apache.kafka.common.serialization.StringSerializer", String.class),
        BYTE_ARRAY_SERIALIZER("org.apache.kafka.common.serialization.ByteArraySerializer", byte[].class);

        private final String className;
        private final Class<?> outputClass;

        Serializer(String className, Class<?> outputClass) {
            this.className = className;
            this.outputClass = outputClass;
        }

        public String getClassName() {
            return className;
        }

        public Class<?> getOutputClass() {
            return outputClass;
        }
    }
}
