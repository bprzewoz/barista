package com.bprzewoz.barista.config;

import com.bprzewoz.barista.control.ObjectGenerator;
import com.bprzewoz.barista.util.ConfigUtils;
import com.bprzewoz.barista.util.FileUtils;
import com.bprzewoz.barista.util.SchemaUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.avro.Schema;

import java.io.File;
import java.util.Objects;

import static com.bprzewoz.barista.config.BaristaConfig.MESSAGES_DIRECTORY;
import static com.bprzewoz.barista.config.BaristaConfig.SCHEMAS_DIRECTORY;
import static com.bprzewoz.barista.util.JsonUtils.format;

public class CaseConfig {
    public static final String CASE = "case";
    public static final String NAME = "name";
    public static final String TOPIC = "topic";
    public static final String SCHEMA = "schema";
    public static final String MESSAGE = "message";

    private final String name;
    private final String topic;
    private final Schema schema;
    private String message;

    public static CaseConfig from(String path, ObjectGenerator objectGenerator) {
        File file = new File(path);
        return from(file, objectGenerator);
    }

    public static CaseConfig from(File file, ObjectGenerator objectGenerator) {
        Config config = ConfigFactory.parseFile(file).getConfig(CASE);
        return from(config, objectGenerator);
    }

    public static CaseConfig from(Config config, ObjectGenerator objectGenerator) {
        String name = config.getString(NAME);
        String topic = ConfigUtils.getValueOrNull(config, TOPIC);

        Schema schema = null;
        if (config.hasPath(SCHEMA)) {
            String schemaPath = SCHEMAS_DIRECTORY + config.getString(SCHEMA);
            schema = SchemaUtils.readSchema(schemaPath);
        }

        String message = null;
        if (config.hasPath(MESSAGE)) {
            String messagePath = MESSAGES_DIRECTORY + config.getString(MESSAGE);
            message = FileUtils.readFile(new File(messagePath));
        } else if (schema != null) {
            message = format(objectGenerator.generateJson(schema));
        }

        return new CaseConfig(name, topic, schema, message);
    }

    public CaseConfig(String name, String topic, Schema schema, String message) {
        this.name = Objects.requireNonNull(name);
        this.topic = topic;
        this.schema = schema;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }

    public Schema getSchema() {
        return schema;
    }

    public String getMessage() {
        return message;
    }

    void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaseConfig that = (CaseConfig) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "CaseConfig{" +
                "name='" + name + '\'' +
                ", topic='" + topic + '\'' +
                ", schema=" + schema +
                ", message='" + message + '\'' +
                '}';
    }
}
