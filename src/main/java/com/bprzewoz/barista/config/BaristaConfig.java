package com.bprzewoz.barista.config;

import com.bprzewoz.barista.control.ObjectGenerator;
import com.bprzewoz.barista.util.ConfigUtils;
import com.bprzewoz.barista.util.FileUtils;
import com.bprzewoz.barista.util.StringUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import org.apache.avro.Schema;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static com.bprzewoz.barista.config.CaseConfig.CASE;
import static com.bprzewoz.barista.config.CaseConfig.MESSAGE;
import static com.bprzewoz.barista.util.ConfigUtils.getValueOrNull;
import static com.bprzewoz.barista.util.JsonUtils.format;

public class BaristaConfig {
    public static final String DEFAULT_VALUES = "barista.default";
    public static final String CASES_DIRECTORY = "./config/cases/";
    public static final String SCHEMAS_DIRECTORY = "./config/schemas/";
    public static final String MESSAGES_DIRECTORY = "./config/messages/";

    private final DefaultValues defaultValues;
    private final Map<CaseConfig, File> caseConfigs;
    private final ObjectGenerator objectGenerator;

    public static BaristaConfig from(String path) {
        File file = new File(path);
        return from(file);
    }

    public static BaristaConfig from(File file) {
        Config config = ConfigFactory.parseFile(file);
        return from(config);
    }

    public static BaristaConfig from(Config config) {
        Config defaultConfig = ConfigFactory.parseMap(getValueOrNull(config, DEFAULT_VALUES));
        DefaultValues defaultValues = DefaultValues.from(defaultConfig);

        Integer collectionsSize = getValueOrNull(config, "collectionsSize");
        ObjectGenerator objectGenerator = collectionsSize != null
                ? new ObjectGenerator(collectionsSize)
                : new ObjectGenerator();

        Map<CaseConfig, File> caseConfigs = readCaseConfigs(CASES_DIRECTORY, objectGenerator);
        return new BaristaConfig(defaultValues, caseConfigs, objectGenerator);
    }

    private static Map<CaseConfig, File> readCaseConfigs(String directoryPath, ObjectGenerator objectGenerator) {
        Map<CaseConfig, File> caseConfigs = new HashMap<>();
        File directory = new File(directoryPath);
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                caseConfigs.put(CaseConfig.from(file, objectGenerator), file);
            }
            return caseConfigs;
        } else {
            throw new IllegalArgumentException("Missing messages directory: " + directory.getPath());
        }
    }

    public BaristaConfig(DefaultValues defaultValues, Map<CaseConfig, File> caseConfigs, ObjectGenerator objectGenerator) {
        this.defaultValues = defaultValues;
        this.caseConfigs = caseConfigs;
        this.objectGenerator = objectGenerator;
    }

    public List<String> getNames() {
        return caseConfigs.keySet()
                .stream()
                .map(caseConfig -> caseConfig.getName())
                .collect(Collectors.toList());
    }

    public void changeMessage(String name, String message) {
        CaseConfig caseConfig = getCaseConfig(name);
        caseConfig.setMessage(message);

        File configFile = getFile(caseConfig);
        Config config = ConfigFactory.parseFile(configFile);
        String messageFilename = name + "_" + StringUtils.generateTimestamp() + ".txt";
        Config modifiedConfig = config.withValue(CASE + "." + MESSAGE, ConfigValueFactory.fromAnyRef(messageFilename));

        FileUtils.writeFile(configFile, ConfigUtils.toString(modifiedConfig));
        FileUtils.writeFile(MESSAGES_DIRECTORY + messageFilename, message);
    }

    public String removeMessage(String name) {
        CaseConfig caseConfig = getCaseConfig(name);
        Schema schema = caseConfig.getSchema();

        String message = null;
        if (schema != null) {
            Object object = objectGenerator.generateJson(schema);
            message = format(object);
        }
        caseConfig.setMessage(message);

        File file = getFile(caseConfig);
        Config config = ConfigFactory.parseFile(file);
        Config modifiedConfig = config.withoutPath(CASE + "." + MESSAGE);
        FileUtils.writeFile(file, ConfigUtils.toString(modifiedConfig));
        return message;
    }

    public CaseConfig getCaseConfig(String name) {
        return caseConfigs.keySet()
                .stream()
                .filter(caseConfig -> Objects.equals(name, caseConfig.getName()))
                .findFirst()
                .orElse(null);
    }

    public File getFile(CaseConfig caseConfig) {
        return caseConfigs.get(caseConfig);
    }

    public DefaultValues getDefaultValues() {
        return defaultValues;
    }
}
