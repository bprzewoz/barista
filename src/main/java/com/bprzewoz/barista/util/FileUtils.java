package com.bprzewoz.barista.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {
    public static final Charset UTF_8 = StandardCharsets.UTF_8;

    private FileUtils() {
        throw new UnsupportedOperationException();
    }

    public static String readFile(String path) {
        return readFile(Paths.get(path));
    }

    public static String readFile(File file) {
        return readFile(file.toPath());
    }

    public static String readFile(Path path) {
        try {
            return new String(Files.readAllBytes(path), UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Reading file failed: " + path, e);
        }
    }

    public static void writeFile(String path, String content) {
        writeFile(Paths.get(path), content);
    }

    public static void writeFile(File file, String content) {
        writeFile(file.toPath(), content);
    }

    public static void writeFile(Path path, String content) {
        try {
            Files.write(path, content.getBytes());
        } catch (IOException e) {
            throw new IllegalStateException("Writing file failed: " + path, e);
        }
    }
}
