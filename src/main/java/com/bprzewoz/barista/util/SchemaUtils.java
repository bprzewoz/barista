package com.bprzewoz.barista.util;

import org.apache.avro.Schema;
import org.apache.avro.SchemaParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public final class SchemaUtils {
    private SchemaUtils() {
        throw new UnsupportedOperationException();
    }

    public static Schema readSchema(String path) {
        File file = new File(path);
        return readSchema(file);
    }

    public static Schema readSchema(File file) {
        try {
            return new Schema.Parser().parse(file);
        } catch (SchemaParseException e) {
            throw new IllegalArgumentException("Invalid schema file:  " + file, e);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Missing schema file: " + file, e);
        } catch (IOException e) {
            throw new IllegalStateException("Read schema failed: " + file, e);
        }
    }
}
