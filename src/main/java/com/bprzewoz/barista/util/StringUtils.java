package com.bprzewoz.barista.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtils {
    public static final String DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";

    private StringUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean isBlank(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isNotBlank(String string) {
        return !isBlank(string);
    }

    public static String generateTimestamp() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat.format(date);
    }
}
