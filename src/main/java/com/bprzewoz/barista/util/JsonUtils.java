package com.bprzewoz.barista.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;

public final class JsonUtils {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private JsonUtils() {
        throw new UnsupportedOperationException();
    }

    public static String format(Object object) {
        return format(object.toString());
    }

    public static String format(String json) {
        try {
            JsonNode jsonNode = objectMapper.readTree(json);
            ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
            return objectWriter.writeValueAsString(jsonNode);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Formatting json failed: " + json, e);
        }
    }
}
