package com.bprzewoz.barista.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigRenderOptions;

public class ConfigUtils {
    private ConfigUtils() {
        throw new UnsupportedOperationException();
    }

    public static <T> T getValueOrNull(Config config, String path) {
        if (config != null && config.hasPath(path)) {
            return (T) config.getAnyRef(path);
        } else {
            return null;
        }
    }

    public static String toString(Config config) {
        return config.root().render(
                ConfigRenderOptions
                        .defaults()
                        .setOriginComments(false)
                        .setComments(false)
                        .setFormatted(true)
                        .setJson(false)
        );
    }
}
