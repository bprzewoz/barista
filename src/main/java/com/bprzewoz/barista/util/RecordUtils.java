package com.bprzewoz.barista.util;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class RecordUtils {
    private RecordUtils() {
        throw new UnsupportedOperationException();
    }

    public static byte[] serialize(GenericRecord genericRecord) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            DatumWriter<GenericRecord> datumWriter = new SpecificDatumWriter(genericRecord.getSchema());
            BinaryEncoder binaryEncoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream, null);
            datumWriter.write(genericRecord, binaryEncoder);
            binaryEncoder.flush();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Serializing failed: " + genericRecord, e);
        }
    }
}
