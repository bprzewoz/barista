package com.bprzewoz.barista;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class Barista extends Application {
    public static final String TITLE = "Barista";
    public static final String LAYOUT = "view/layout.fxml";
    public static final String STYLESHEET = "view/stylesheet.css";

    public static void main(String[] args) {
        Barista.launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        URL location = getClass().getClassLoader().getResource(LAYOUT);
        FXMLLoader fxmlLoader = new FXMLLoader(location);

        double width = Screen.getPrimary().getVisualBounds().getWidth() / 3;
        double height = Screen.getPrimary().getVisualBounds().getHeight() - 100;
        Scene scene = new Scene(fxmlLoader.load(), width, height);
        scene.getStylesheets().add(STYLESHEET);
        Image icon = new Image(Barista.class.getResourceAsStream("/images/barista32x32.png"));

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((screenBounds.getWidth() - width) / 2);
        stage.setY((screenBounds.getHeight() - height) / 2);
        stage.getIcons().add(icon);
        stage.setScene(scene);
        stage.setTitle(TITLE);
        stage.show();
    }
}
