# 🧔 Barista

Postman for Kafka!

## Introduction

Barista is a desktop application which allows friendly communication with Kafka.

It means sending messages in a similar way to Postman tool.

## Functionality

### Sending messages

The basic function of the application is sending messages to Kafka.

It is possible to parametrize an address, port and topic during the sending process.

![send](gif/send.gif)

It is possible to define the number of sent messages.

![sendMany](gif/sendMany.gif)

### Disruption in the sending process

Sending process may be stopped at any moment.

![stop](gif/stop.gif)

### Saving messages

Prepared messages may be saved and stored in text files.

![save](gif/save.gif)

### Resetting messages

Saved messages may be reset to a default value. 

![drop](gif/drop.gif)

## Types

It is possible to send two types of message.

### String

![string](gif/string.gif)

### Record

Java class object is created on Avro scheme basis and sent in a form of bytes array. 

It works only for test cases which passes a path to the correct scheme.

Default messages are generated based on schemes when starting.

![record](gif/record.gif)

## Configuration

Configuration is divided into application configuration and test case configuration.

### Application configuration

The following fields may be set in the application configuration:

* **bootstrapServers** *(optional)* - server address,
* **topic** *(optional)* - default topic,
* **duration** *(optional)* - sending time,
* **capacity** *(optional)* - sending capacity.

Example application configuration:

```
barista {
    default {
        bootstrapServers = "localhost:9092"
        topic = "test"
        duration = "1"
        capacity = "1"
    }
}
```
 
### Case configuration

The following fields may be set in the case configuration:

* **name** - name of the case,
* **topic** *(opcjonalne)* - name of the topic,
* **schema** *(opcjonalne)* - path to the file with a scheme,
* **message** *(opcjonalne)* - path to the file with a message.

Example case configuration:

```
case {
    name="RecordCase"
    topic="RecordTopic"
    schema="schema.avsc"
    message="RecordMessage"
}
```

## Plans

Following issues have to be done:

* add unit tests,
* move notification view to a separate file.